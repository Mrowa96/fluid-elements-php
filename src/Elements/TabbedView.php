<?php
    namespace fluid\elements;

    class TabbedView
    {
        private $document;
        private $wrapper;
        private $controls;
        private $sections;
        private $sectionsList = [];
        private $defaultSection = null;
        private $defaultConfiguration = null;

        public function __construct()
        {
            libxml_use_internal_errors(true);

            $this->document = new \DOMDocument('1.0', "UTF-8");
            $this->document->preserveWhiteSpace = true;
            $this->document->formatOutput   = true;
            $this->defaultConfiguration = "element: tabbedView, default: first,";

            $this->createWrapperBlock();
            $this->createControlsBlock();
            $this->createSectionsBlock();
        }
        public function addControl($data, $section = null, $params = [])
        {
            if($section !== null){
                if(in_array($section, $this->sectionsList) === false){
                    $this->sectionsList[] = $section;
                    $control = $this->document->createElement("li");

                    $control->setAttribute("data-trigger", $section);
                    $this->handleParams($params, $control);

                    $this->appendHTML($control, $data);
                    $this->controls->appendChild($control);
                }
            }
            else{
                $control = $this->document->createElement("li");
                $this->handleParams($params, $control);
                $this->appendHTML($control, $data);
                $this->controls->appendChild($control);
            }

            return $this;
        }
        public function addControls($controls)
        {
            if(!empty($controls)){
                foreach($controls AS $control){
                    if(is_array($control)){
                        if(isset($control['data'])){
                            $section = isset($control['section']) ? $control['section'] : null;
                            $params = isset($control['params']) ? $control['params'] : [];

                            $this->addControl($control['data'], $section, $params);
                        }
                        else{
                            throw new \Exception("You must provide data key in controls array.", 10);
                        }
                    }
                    else{
                        throw new \Exception("Control must be in array format and contain data key.", 11);
                    }
                }
            }

            return $this;
        }
        public function addSection($html, $name, $params = [])
        {
            if(in_array($name, $this->sectionsList) === true) {
                $section = $this->document->createElement("section");

                $section->setAttribute("data-section", $name);
                $this->handleParams($params, $section);

                $this->appendHTML($section, $html);
                $this->sections->appendChild($section);
            }
            else{
                throw new \Exception("Cannot find '".$name."' in sections list, add control first.", 20);
            }

            return $this;
        }
        public function addSections($sections)
        {
            if(!empty($sections)){
                foreach($sections AS $section){
                    if(is_array($section)){
                        if(isset($section['data']) && isset($section['section'])){
                            $sectionName = isset($section['section']) ? $section['section'] : null;
                            $params = isset($section['params']) ? $section['params'] : [];

                            $this->addSection($section['data'], $sectionName, $params);
                        }
                        else{
                            throw new \Exception("You must provide data and section keys in sections array.", 30);
                        }
                    }
                    else{
                        throw new \Exception("Section must be in array format and contain data key.",31);
                    }
                }
            }

            return $this;
        }
        public function setDefault($section)
        {
            if(in_array($section, $this->sectionsList) === true) {
                if($this->defaultSection === null){
                    $this->defaultConfiguration =  preg_replace(
                        "/default\:\s[a-zA-Z0-9]+/",
                        "default: ".$section."",
                        $this->defaultConfiguration
                    );
                    $this->defaultSection = $section;
                }
                else{
                    throw new \Exception("Default section already was set.", 40);
                }
            }
            else{
                throw new \Exception("Cannot find '".$section."' in sections list, add control first.", 41);
            }

            return $this;
        }
        public function render()
        {
            $this->wrapper->setAttribute("data-fluid", $this->defaultConfiguration);

            echo $this->document->saveHTML();
        }

        protected function handleParams($params, \DOMElement $element)
        {
            if(!empty($params)){
                foreach ($params AS $key => $value){
                    $element->setAttribute($key, $value);
                }
            }
        }
        protected function appendHTML(\DOMNode $parent, $source)
        {
            $tmpDoc = new \DOMDocument();
            $tmpDoc->loadHTML(mb_convert_encoding($source, 'html-entities', 'utf-8'));
            $docBody = $tmpDoc->getElementsByTagName('body')->item(0);

            foreach ($docBody->childNodes as $node) {
                $newNode = $parent->ownerDocument->importNode($node, true);
                $parent->appendChild($newNode);
            }
        }

        private function createWrapperBlock()
        {
            $this->wrapper = $this->document->createElement("div");

            $this->document->appendChild($this->wrapper);
        }
        private function createSectionsBlock()
        {
            $this->sections = $this->document->createElement("div");
            $this->sections->setAttribute("class", "sections");

            $this->wrapper->appendChild($this->sections);
        }
        private function createControlsBlock()
        {
            $this->controls = $this->document->createElement("ul");
            $this->controls->setAttribute("class", "controls");

            $this->wrapper->appendChild($this->controls);
        }
    }